// Copyright (c) 2023, The Tor Project, Inc.
// See LICENSE for licensing information.

fn main() {
    let hashx_src_files = vec![
        "c_src/equix/hashx/src/blake2.c",
        "c_src/equix/hashx/src/compiler.c",
        "c_src/equix/hashx/src/compiler_a64.c",
        "c_src/equix/hashx/src/compiler_x86.c",
        "c_src/equix/hashx/src/context.c",
        "c_src/equix/hashx/src/hashx.c",
        "c_src/equix/hashx/src/hashx_thread.c",
        "c_src/equix/hashx/src/hashx_time.c",
        "c_src/equix/hashx/src/program.c",
        "c_src/equix/hashx/src/program_exec.c",
        "c_src/equix/hashx/src/siphash.c",
        "c_src/equix/hashx/src/siphash_rng.c",
        "c_src/equix/hashx/src/virtual_memory.c",
    ];

    cc::Build::new()
        .files(hashx_src_files)
        .include("c_src/equix/hashx/include")
        .compile("hashx");
}
