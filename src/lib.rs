mod ffi {
    use core::ffi::c_void;

    #[repr(C)]
    pub struct HashXContext {
        _data: [u8; 0],
        _marker: core::marker::PhantomData<(*mut u8, core::marker::PhantomPinned)>,
    }

    extern "C" {
        pub fn hashx_alloc(hashx_type: u8) -> *mut HashXContext;
        pub fn hashx_free(hashx_ctx: *mut HashXContext);
        pub fn hashx_make(hash_ctx: *mut HashXContext, seed: *const c_void, size: usize);
        pub fn hashx_exec(hash_ctx: *mut HashXContext, input: u64, output: *mut c_void) -> i32;
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        unsafe {
            let ctx = ffi::hashx_alloc(0);
            ffi::hashx_free(ctx);
        }

        assert_eq!(1337, 1337);
    }
}
