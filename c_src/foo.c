/* Copyright (c) 2023, The Tor Project, Inc. */
/* See LICENSE for licensing information */

int foo(void) {
  return 1337;
}
